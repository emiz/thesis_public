---
title: Changelog
format:
  gfm:
    output-file: changelog.md
  html:
    number-sections: false
format-links: false
---

{{< include _new_changes.qmd >}}

:::{.content-visible when-format="html"}
{{< include _old_changes.qmd >}}
:::