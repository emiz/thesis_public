## Electron Train Behavior in LZ {#sec-se-me-trains}

This work will summarize the basic characteristics of electron trains in LZ. An example of a typical electron train in LZ is shown in @fig-etrain-wf-01.

::: {#fig-etrain-wf-01 layout-nrow="2"}
![A single scatter event in LZ. The progenitor S2 pulse at 6.2 ms is followed by a tail of misreconstructed pulses from photoionization up to approximately 7.5 ms. As the excess light subsides, SEs (SE, marked with red dots) are visible until the event ends.](figs/etrain_gif/etrainwf0.png){#fig-etrain-wf-0}

![In the following event, more SE pulses are visible.](figs/etrain_gif/etrainwf1.png){#fig-etrain-wf-1}

The first two events of an electron train in LZ, showing the presence of SEs for multiple milliseconds after an S2 pulse.
:::

PMT hitmaps from both events reveal that the SEs in the following event are co-located with the lateral position of the progenitor S2.

::: {#fig-etrain-hm-01 layout-nrow="2"}
![PMT hitmap from the event shown in @fig-etrain-wf-0. The excess of light at the 3 o'clock position in the top PMT array corresponds to the progenitor S2](figs/etrain_gif/etrainhm0.png){#fig-etrain-hm-0 width="70%"}

![PMT hitmap from the event shown in @fig-etrain-wf-1. Light is still detected in the TPC in the form of stray single photoelectrons (SPEs) and SEs. The persistent excess of light at the 3 o'clock position is due to SEs which constitute an electron train.](figs/etrain_gif/etrainhm1.png){#fig-etrain-hm-1 width="70%"}

PMT hitmaps from the first and second events shown in @fig-etrain-wf-01. Hitmaps on the left correspond to the top PMT array, while hitmaps on the right correspond to the bottom PMT array.
:::

For this analysis, progenitors are S2s at least 200 extracted electrons in size to induce significant delayed electron emission, and must be part of a single scatter event. A drift time cut of 50 µs to 950 µs and a radial cut of 55 cm are used to select progenitors with well-understood energy and position properties. The drift time cut follows from the fiducial cut implemented in LZ's first science run, SR1 @aalbers2023, while the radial cut removes "glue-ring" events which originate from radioactive decays on the gate grid at high radii but may have their positions mis-reconstructed. After each progenitor, all small electron pulses are identified in randomly triggered data acquisition windows, and the process stops when a vetoing pulse (any pulse larger than 100 extracted electrons) is detected. Progenitors followed by a vetoing pulse within 5 ms are discarded in order to ensure that a sufficient amount of time has elapsed to observe SEs. Progenitors also must be at least 200 ms after any vetoing pulse to mitigate the aftermath of earlier energy depositions. As an additional precaution, progenitors are required to end at least 1 µs before the end of an event window to ensure that they are properly resolved. Finally, events with otherwise valid progenitors are rejected if any data buffer for storing PMT digitizations is saturated. This cut effectively excludes progenitor pulses larger than 10^6^phd and largely mitigates the effects of PMT saturation. Additional technical details on the data extraction algorithm and theory of pulse rate measurement are given in @sec-alpaca-algo.

Plots shown in this work are created using data from either SR1, or runs 6726-6756 which were part of the pre-SR1 commissioning campaign. Grid voltages in both datasets were identical; the anode, gate, and cathode voltages were -4,-4, and -32 kV respectively. This corresponded to a nominal liquid extraction field of 3.9 kV/cm and drift liquid field of 193 V/cm @aalbers2023. A key difference between each set of data are the trigger settings. SR1 utilized an S2-based trigger which had a trigger efficiency of \~100% for S2 pulses \> 5 SE in size, and approximately 10% for SE pulses @aalbers2023. Meanwhile, runs 6726-6756 utilized a purely random \~87 Hz trigger with 11.1 ms long event windows, resulting in a pulse-agnostic \~95% livetime. Despite this difference in trigger configurations, the similar SE trigger efficiencies resulted in a negligible impact to SE rate measurements in SR1 data. Nevertheless, data from runs 6726-6756 is presented in this article unless the high statistics from SR1 were necessary.

Finally, data in this and other work is normalized by combinations of the following parameters shown in @tbl-rate-norms, depending on the circumstance.

| Factor                                       | Description                                                         |
|--------------------------|----------------------------------------------|
| $e_{R} = S2_{phd} / n_{phd / SE}$            | "Raw" (extracted) S2 area in units of SEs                           |
| $e_{S} = e_{R} / e_{eee}$                    | "Surface" S2 area, i.e. $e_{R}$ corrected for extraction efficiency |
| $e_{I} = e_{S} \exp(t_{drift} / \tau_{e^-})$ | "Initial" S2 area, i.e. $e_{S}$ corrected for drift losses          |
| $e_{L} = e_{I} - e_{S}$                      | Number of electrons lost while drifting                             |
| $\text{cm}^2$                                | Area of liquid surface subtended by radial selection of pulses      |

: Table of child pulse rate normalization factors. {#tbl-rate-norms}

### Position and Time Dependence of SEs {#sec-se-me-dt-dr-dep}

This position and time dependence is a hallmark of electron trains which are known to persist for at least a second after an S2 occurs. This can be seen in the SE rate relative to an S2, shown in @fig-se-me-child-dr-cm-se.

::: {#fig-se-me-child-dr-cm-se layout="[-7.5,85,-7.5]"}
{{< embed se_me_trains.ipynb#se-me-child-dr-cm-se >}}

The rate of SEs after S2s, per square centimeter of liquid surface in the TPC. The x-axis is the radial distance between between the child SE pulse and the most recent progenitor S2 pulse. The stack axis denotes the time since the end of the progenitor S2.
:::

The stark disparity in rates within 10 cm of a progenitor lends itself to the definition of "position-correlated" and "position-uncorrelated" pulses (@tbl-pcorr-defs):

|   Position   | Distance to Progenitor \[cm\] |
|:------------:|:-----------------------------:|
|  Correlated  |            $< 10$             |
| Uncorrelated |     $20 < \Delta r < 30$      |

: Definitions of position-correlated and uncorrelated child pulses. {#tbl-pcorr-defs}

These definitions were chosen by eye to capture the rates of each population without leakage between the two. Furthermore, the position-uncorrelated region was chosen to only minimally extend beyond the walls of the TPC in the worst-case where a progenitor occurs at a radius of 55 cm. To further correct for this scenario, an additional scaling factor is applied to counts of SE pulses at $\Delta$r which are commensurate with a circumference that extends beyond the TPC.

This scaling factor is inversely proportional to the fraction of the circumference of the circle defined by $\Delta$r that is contained inside the TPC. The intent of this correction rests on the assumption that a uniform number of counts per unit distance are expected along the circle defined by $\Delta$r. If a portion of this circle exists beyond the walls of the TPC, the additional weighting will appropriately scale up the number of detected counts at the value of $\Delta$r, effectively simulating a TPC with an infinitely large radius. The effects of this correction are minor for $\Delta$r up to approximately 30 cm, but result in a drastic flattening of the position-uncorrelated SE rate at higher $\Delta$r. This correction is especially elegant because it allows for progenitors to be treated on equal footing, regardless of their position in the TPC, eliminating the need for specialized corrections to livetime.

![Diagram showing geometric constraints needed to correct for the lack of pulses observed relative to a progenitor when pulses are observed at a radius $R_{SE}$ which extends beyond the walls of the TPC. A child pulse may be observed anywhere inside the TPC along the circle defined by $R_{SE}$, Counts of pulses along this arc are then used to correct for the lack of pulses observed along the arc outside of the TPC.](figs/child_dr_weight.svg){#fig-child-dr-weight width="70%"}

A diagram detailing this scaling factor is shown in @fig-child-dr-weight. For a progenitor at radius $R_{prog}$ inside a TPC with radius $R_{TPC}$, and a SE pulse at $R_{SE}$ relative to the progenitor, the following are always true:

A progenitor must be found within the bounds of the TPC:

$$
0 \leq R_{Prog} < R_{TPC}
$$

And the farthest away a SE can be found is at the opposing end of the TPC:

$$
0 \leq R_{SE} < R_{TPC} + R_{Prog}
$$

The angle $\theta$ defined in @fig-child-dr-weight subtends half of the arc inside of the TPC. Thus, the fraction of the circumference of the circle inside of the TPC is equal to:

$$
\frac{2\theta}{360}
$$

While $\theta$ is found using the law of cosines:

$$ 
\theta = \arccos(\frac{R_{SE}^2 + R_{Prog}^2 - R_{TPC}^2 }{ 2 R_{SE} R_{Prog}}) 
$$

The position-correlated region captures a power law in the rate vs time, while the position-uncorrelated rate decays much faster (@fig-se-me-se-pcorr-puncorr).

::: {#fig-se-me-se-child-pcorr-rate-fit layout="[[-7.5,85,-7.5],[-7.5,85,-7.5]]"}
::: {#fig-se-me-se-pcorr-puncorr}
{{< embed se_me_trains.ipynb#se-me-se-pcorr-puncorr >}}

Rates of position-correlated and position-uncorrelated SE pulses at time $\Delta t$ after a progenitor S2 pulse. The rates are converted to a flux using the definitions given in @tbl-pcorr-defs.
:::

::: {#fig-se-me-se-rate-fit}
{{< embed se_me_trains.ipynb#se-me-se-rate-fit >}}

@fig-se-me-se-pcorr-puncorr with power law fit to rates of position-correlated SEs. The power law exponent is consistent with results from LUX @akerib2020 and XENON1T @xenoncollaboration2022. A timescale of 3 ms to 200 ms was chosen for the fit range. This range was chosen because the position-correlated rates are dominant in this region, to the extent that subtracting the position-uncorrelated rates has a negligible effect on the fit. Prior to 3 ms the slope is steeper from residual photoionization effects. After 200 ms, the power-law slope is shallower due to contamination from position-uncorrelated delayed electrons.
:::

Rates of position-correlated and uncorrelated SE pulses versus time since the end of their progenitor S2. The rates prior to 1 ms are a known artifact of pulse pile-up from photoionization (see also @fig-photoion-check-prog-area-stack). Position-uncorrelated rates drop off quickly after the full drift time has elapsed, in comparison to the rates from position-correlated pulses.
:::

A power law of the following form can be fit to the position-correlated rates, as shown in @fig-se-me-se-rate-fit:

$$
\alpha t ^{\beta}
$$ {#eq-power-law}

A timescale of 3 ms to 200 ms was chosen for the fit range. 3 ms was chosen to avoid residual photoionization effects, while 200 ms was chosen as an upper bound because the power-law slope is shallower due to contamination from position-uncorrelated delayed electrons. The large difference in position-correlated and uncorrelated rates between 3-200 ms also obviates the need for background subtraction because the change in the position-correlated rate after subtraction is negligible in this region.

It is also worth emphasizing that the drop in the rates prior to 1 ms is a known artifact of pulse pile-up from photoionization. An example of this is visible in @fig-etrain-wf-0 where many of the S2 pulses immediately after the progenitor S2 are piled-up SEs. his effect can be seen more concretely in @fig-photoion-check-prog-area-stack when examining rates of S2 pulses after progenitors, instead of SEs. Unlike with SEs, S2 pile-up does not leak into a different classification in LZ. Rates may still be distorted because counts of S2s can be reduced, but the S2 classification is ultimately more resilient against the effects of pile-up.

::: {#fig-photoion-check-prog-area-stack}
::: {.content-visible when-format="html"}
::: {.figure-content .column-page}
{{< embed se_photoionization.ipynb#photoion-check-prog-area-stack >}}
:::
:::

::: {.content-visible when-format="pdf"}
::: figure-content
{{< embed se_photoionization.ipynb#photoion-check-prog-area-stack >}}
:::
:::

Rates of position-correlated SE and S2 pulses versus the size of their respective progenitors. For the smallest progenitors, SE rates appear to decay monotonically. As the progenitor size increases, pulses lose their SE classification, causing a distortion in the rate prior to 1 ms. This feature is not present when selecting for S2 pulses because pulse classification is conserved for S2s undergoing pile-up.
:::

### Progenitor Size, Drift Time, and Electron Lifetime Dependence

The rates of delayed position-correlated SEs depend on the size of the progenitor pulse, while position-uncorrelated SEs do not (@fig-se-me-prog-area-dep-se-child-pcorr).

::: {#fig-se-me-prog-area-dep-se-child-pcorr layout="[-15,70,-15]"}
{{< embed se_me_trains.ipynb#se-me-prog-area-dep-se-child-pcorr >}}

Rates of SEs versus the size of their progenitor, $e_{R}$, in the number of electrons extracted. A time window of 3-300 ms was chosen to avoid integrating any residual photoionization. The position-uncorrelated rates do not exhibit a dependence on the size of the progenitor; this behavior was also observed in XENON1T @xenoncollaboration2022. Most progenitors are between 10^3^ and 10^4^ $e_{R}$ in size; SR1 data was used in this plot to maximized the available statistics.
:::

Progenitors drifting through more liquid have higher rates of delayed electrons following them. In @fig-se-me-prog-area-dep-se-child-pcorr rates are normalized by $e_{I}$, the size of the progenitor at the interaction site, in units of SEs. $e_{I}$ is a function of the drift time $t_{drift}$ and electron lifetime $\tau_e$:

$$
e_{I} = e_{S} \exp(t_{drift} / \tau_{e^-})
$$

It is proportional to the size of the progenitor at the liquid surface $e_{S}$, which is the size of the progenitor corrected for the extraction efficiency $e_{eee}$:

$$
e_{S} = e_{R} / e_{eee}
$$

::: {#fig-se-me-prog-drift-time-dep-se-child-pcorr layout="[-15,70,-15]"}
{{< embed se_me_trains.ipynb#se-me-prog-drift-time-dep-se-child-pcorr >}}

Rates of SEs versus the drift time of their progenitor. The position-correlated rate appears to linearly depend on the drift time of the progenitor even after normalizing by $e_{I}$, the size of the progenitor at the interaction site. The position-uncorrelated rates do not exhibit a dependence on the drift time of the progenitor. This behavior was also observed in XENON1T @xenoncollaboration2022, and highlighted as an indication that uncorrelated delayed electrons may be correlated with previous progenitors. SR1 data was used in this plot to maximized the available statistics.
:::

It is worth highlighting here the lack of correlation exhibited by position-uncorrelated pulse rates in both @fig-se-me-prog-area-dep-se-child-pcorr and @fig-se-me-prog-drift-time-dep-se-child-pcorr. This would indicate that position-uncorrelated pulses originate from some source other than the most recent progenitor. This is compatible with the explanation put forward by the XENON1T collaboration that these may be delayed electrons from previous electron trains @xenoncollaboration2022.

@fig-se-me-drift-time-unnorm shows the same data from @fig-se-me-prog-drift-time-dep-se-child-pcorr except with rates plotted against the time since the progenitor S2. Counterintuitively, the position-correlated photoionization rates in @fig-se-me-drift-time-unnorm following progenitors at lower drift times also appear to be lower. This is unexpected because with fewer electrons lost, S2s should be larger and the TPC should experience more photoionization. A possible explanation for this could be that a suppression of the observed SE rate occurs due to a increased pile-up rate (see again @fig-photoion-check-prog-area-stack). This seems unlikely though, as LUX @akerib2020 observed this even when small progenitors were explicitly studied to avoid the effects of pile-up masking SEs. Position-correlated photoionization rates are also lower at lower electron lifetimes (@fig-se-me-prog-e-lifetime-stack-child-dt), but this feature is less straightforward to understand because while more electrons are lost at lower electron lifetimes, more impurities are present which could be photoionized in the TPC, leading to pile-up suppression of the SE pulse classification. As this work focused on the behavior of delayed electrons, this question remains unaddressed, but may be the subject of future work.

::: {#fig-se-me-drift-time-unnorm}

::: {.content-visible when-format="html"}
::: {.column-page}
{{< embed se_me_trains.ipynb#se-me-drift-time-unnorm >}}
:::
:::

::: {.content-visible when-format="pdf"}
{{< embed se_me_trains.ipynb#se-me-drift-time-unnorm >}}
:::

Drift time dependence of SE rates versus time since the progenitor S2. The position-uncorrelated rates do not exhibit a clear drift time dependence at any timescale. Position-correlated photoionization rates have a counterintuitive dependence on the drift time. At shorter drift times, S2s should lose fewer electrons and photoionize the TPC more. However, lower photoionization rates are observed in this case, and this phenomenon was also observed in LUX @akerib2020. This behavior remains unexplained, and should be the subject of future work.
:::


During SR1, the electron lifetime reached 8 ms, offering a unique opportunity to measure the SE rate dependence at electron lifetimes that had not previously been measured. @fig-se-me-e-lifetime-dep-se-child-pcorr shows that the rate decreases with larger electron lifetimes, although the effect tapers off at higher electron lifetimes. This behavior is in agreement with that observed by XENON1T, where it was taken as an indication that the species responsible for electron trains is distinct from typical electronegative impurities like oxygen that affect the electron lifetime at all concentrations. The reason for the sharp drop in the position-correlated rate at electron lifetimes > 5 ms is unknown. A possible reason could be the sampling of electron lifetime data in LZ multiple days apart. Electron lifetime data in the plot was generated by interpolating an empirical plot of electron lifetime versus run-time, and while it was typically stable over time, several temporary losses of of purity occurred. On these occasions the purity recovered quickly--much faster than the electron lifetime sampling rate--likely leading to an inaccurate interpolation of electron lifetime values. Meanwhile, the position-uncorrelated rate exhibits a slight dependence on the electron lifetime. This is to be expected if the position-uncorrelated SEs are from prior progenitors; the electron lifetime does not vary on the sub-second timescale between progenitors. 

::: {#fig-se-me-e-lifetime-dep-se-child-pcorr layout="[-15,70,-15]"}
{{< embed se_me_trains.ipynb#se-me-e-lifetime-dep-se-child-pcorr >}}

Rates of SEs verses the electron lifetime recorded at the time of the progenitor pulse. The rate at higher electron lifetime values appears to taper off, in agreement with behavior observed by XENON1T @xenoncollaboration2022 and their explanation that the impurity or impurities responsible for electron trains does not perfectly track the concentration of impurities which affect the electron lifetime. A sharp drop in the position-correlated rate is apparent after the electron lifetime exceeds 5 ms. This may be an artifact of interpolating electron lifetime data, as explained in the text. The position-uncorrelated rate also exhibits a slight dependence on the electron lifetime, which would be consistent with the explanation from XENON1T @xenoncollaboration2022 that these backgrounds are mostly from prior electron trains.
:::

::: {#fig-se-me-prog-e-lifetime-stack-child-dt}

::: {.content-visible when-format="html"}
::: {.column-page}
{{< embed se_me_trains.ipynb#se-me-prog-e-lifetime-stack-child-dt >}}
:::
:::

::: {.content-visible when-format="pdf"}
{{< embed se_me_trains.ipynb#se-me-prog-e-lifetime-stack-child-dt >}}
:::

Electron lifetime dependence of SE rates versus time since the progenitor S2. A weak dependence on the electron lifetime is visible for both the position-correlated and uncorrelated rates at all timescales after the full drift time. The position-uncorrelated rate in the 2-4 ms electron lifetime bin is noticeably uneven due to wrongly associated pairs of SEs and progenitor S2s. This occurs for all other position-uncorrelated rates in both this figure and @fig-se-me-drift-time-unnorm, but the effect is enhanced with lower statistics.
:::

### Electron Loss Normalization {#sec-e-loss-norm}

The drift time (@fig-se-me-prog-drift-time-dep-se-child-pcorr) and electron lifetime (@fig-se-me-e-lifetime-dep-se-child-pcorr) dependence persist in the SE rates despite normalization by $e_{I}$. These rates over time are explicitly shown in @fig-se-me-drift-time-unnorm and @fig-se-me-prog-e-lifetime-stack-child-dt, which each compare the rates of position-correlated and position-uncorrelated SEs. 

A common hypothesis on the origin of electron trains is that they are generated from the release of electrons by electronegative impurities. After normalization by $e_{I}$ however, the rates still vary linearly with drift time (@fig-se-me-prog-drift-time-dep-se-child-pcorr). Therefore, it is reasonable to consider an alternative normalization that better represents the mechanism responsible for the drift time dependence.

As it drifts through the liquid, a progenitor S2 loses electrons to electronegative impurities. The number of electrons lost, $e_{L}$, should be the difference between the number of electrons at the interaction site and at the surface:

$$
e_{L} = e_{I} - e_{S}
$$

Rates of SEs after normalizing by $e_{L}$ instead of $e_{I}$ are shown in @fig-se-me-elife-drift-time-normd. This normalization negates both the drift time and electron lifetime dependence effectively unifying both. It further demonstrates the crucial role that electronegative impurities play in the formation of electron trains.

::: {#fig-se-me-elife-drift-time-normd}
::: {#fig-se-me-drift-time-e-loss-norm}
::: {.content-visible when-format="html"}
::: column-page
{{< embed se_me_trains.ipynb#se-me-drift-time-e-loss-norm >}}
:::
:::

::: {.content-visible when-format="pdf"}
{{< embed se_me_trains.ipynb#se-me-drift-time-e-loss-norm >}}
:::

Drift time dependence of SE rates versus time since the progenitor S2. The 50-230 µs band remains elevated above the other bands, indicating that disproportionately fewer electrons may be lost in this region. It is unclear as to why this may be the case. Electric field non-uniformity is not plausible, as the drift field is well-established within a drift time of a few  µs.
:::

::: {#fig-se-me-prog-e-lifetime-stack-child-dt-e-loss-norm}
::: {.content-visible when-format="html"}
::: column-page
{{< embed se_me_trains.ipynb#se-me-prog-e-lifetime-stack-child-dt-e-loss-norm >}}
:::
:::

::: {.content-visible when-format="pdf"}
{{< embed se_me_trains.ipynb#se-me-prog-e-lifetime-stack-child-dt-e-loss-norm >}}
:::

Electron lifetime dependence of SE rates versus time since the progenitor S2. The stratification of photoionization rates is expected because a larger number of electrons lost will depress rates more at low electron lifetimes, and high drift times.
:::

The same rates from @fig-se-me-drift-time-unnorm and @fig-se-me-prog-e-lifetime-stack-child-dt. instead normalized by $e_{L}$, the number of SEs lost by the progenitor as it drifts through the liquid. Both the drift time dependence and electron lifetime dependence that were visible in @fig-se-me-drift-time-unnorm and @fig-se-me-prog-e-lifetime-stack-child-dt are negated, indicating that impurities which capture electrons in the liquid are primarily responsible for electron trains.
:::