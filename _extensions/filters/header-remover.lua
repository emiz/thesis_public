--[[
Remove a header in a document that uses quarto's
classes (as of v1.4.550). Useful for removing the first
header in a book project (see https://github.com/quarto-dev/quarto-cli/issues/2140)

For example, this header will be removed:

# Preface {.content-hidden when-format="pdf"}

]]
--

function Header(element)
    if (element.classes:includes("content-hidden") and quarto.doc.isFormat(element.attributes["when-format"]))
        or (element.classes:includes("content-visible") and not quarto.doc.isFormat(element.attributes["when-format"])) then
        return {}
    end
end
