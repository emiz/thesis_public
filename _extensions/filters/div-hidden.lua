--[[
Enables a special class that hides a div by unpacking it's contents.

For example:

::: {#fig-main}

::: {.column-page .div-hidden when-format="pdf"}
::: {layout-ncol="2"}
::: {#fig-a .figure-content}
![](cover.png)

Figure A.
:::

::: {#fig-b .figure-content}
![](cover.png)

Figure B.
:::
:::
:::

Main Caption.
:::

Here, the div containing .column-page will be absent when the format is pdf, which is useful
because pdf doesn't support this class.

]]
--

function Div(element)
    if (element.classes:includes("div-hidden") and quarto.doc.isFormat(element.attributes["when-format"]))
        or (element.classes:includes("div-visible") and not quarto.doc.isFormat(element.attributes["when-format"])) then
        return element.content
    end
end
