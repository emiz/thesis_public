# README

Link to website is located in project description. Note that this project uses [Git LFS](#git-lfs) to track files.

## Install Script

`setup.sh` will attempt to install necessary dependencies for building the project (not tested).

## Create a PDF

1. `quarto render` to render the project, including a pdf

The majority of styling guide restrictions are baked in to `_quarto-thesis.yml` and the pandoc template files in `tex/partials`.

Some styling issues remain:

* Acknowledgements need to come before the table of contents.

## Preview Website

1. `quarto preview` to start live preview of website
   1. The defense slides will be available but will NOT be a live preview
2. `quarto preview --profile defense` to start a live preview of the defense slides.
   1. The website will be available but will NOT be a live preview.

## Manually Install Dependencies

### Installing and Updating Quarto

Download the version used to render this project with:

```bash
export QUARTO_VERSION="1.4.550"
sudo curl -o quarto-linux-amd64.deb -L https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-amd64.deb
```

Install or update with:

```bash
sudo dpkg -i quarto-linux-amd64.deb
```

### Installing TinyTex

Quarto can install TinyTex, a distribution of LaTeX, for managing LaTeX publications.

Installing with the following command will add it to `$PATH`.

```bash
quarto install tinytex --update-path
```

Uninstall with:

```bash
quarto uninstall tinytex
```

which deletes ~/.TinyTex and removes symlinks to LaTeX packages in your $PATH. More information [here](https://quarto.org/docs/output-formats/pdf-engine.html#installing-tex).

### Installing Latexindent

Latexindent is a package that can automatically format `.tex` files. It may fail to work and complain that some Perl modules are not installed.

```bash
sudo apt install perl # If you don't have Perl, I guess
sudo cpan -i App::cpanminus #Switch to using cpanminus to handle module installs
sudo cpanm YAML::Tiny # Instally Tiny YAML module
sudo cpanm File::HomeDir # Installs HomeDir module

# Might need this if cpanm fails and says you are missing 'make'
sudo apt install build-essential
```

### Vector Images (PDFs and SVGs)

PDFs in Quarto's HTML output are displayed in an embedded viewing widget, which is useful for embedding documents
but clunky for figures. Consequently, SVG files are use instead of PDFs.
To use svg files in Latex output, Quarto will automatically convert SVGs back to PDFs with `rsvg-convert`.
This is supplied by `librsvg2-2`.

```bash
sudo apt update
sudo apt upgrade
sudo apt install librsvg2-2
sudo apt install librsvg2-bin
```

To convert `pdf` to `svg`, use `pdf2svg`:

```bash
sudo apt install pdf2svg
```

### Mermaid Diagrams

Quarto can handle mermaid diagrams in HTML but rendering them to PDFs complicates the rendering 
environment by requiring chromium. Consequently, mermaid diagrams are compiled and converted to svg locally
to keep the rendering environment functional. Use [`mmd2svg.sh`](mmd2svg.sh) to quickly convert `mmd` to `svg` files.

```bash
# Install NVM to manage installations of NPM which is needed to install mermaid
# for making diagrams
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
source ~/.bashrc
nvm install node
npm install -g @mermaid-js/mermaid-cli
```

## Project Profiles

Project profiles are used to render both the thesis and defense slides sequentially. This is because the
thesis is a "book" type project which cannot render the slides without attempting to include them in the thesis (which fails).

By granting `_quarto.yml` two children, one can be used to configure the defense and one can be used to
configure the thesis. Both will inherit from and override the main configuration file.

## Citation Style Language (CSL) Files

CSL Files are used for setting the reference and citation styling. Many can be [found online](https://www.zotero.org/styles).

## GitLab CI/CD and Docker

This project makes use of GitLab CI/CD (see `.gitlab-ci.yml`) to deploy static content rendered by Quarto
to a GitLab Pages website.

First, `_quarto.yml` is configured to send output to `public`, which GitLab will
look in for deploying to a website.

Second, this project uses a custom Docker image which is configured with Quarto and TinyTex so the pipeline can
run `quarto render` and build the pdf instead of having to commit
it to the repository.

## Git LFS

This repository has a lot of binary files, namely pictures. As such, they are tracked with [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/#lfs-objects-in-project-archives) to alleviate the problem of committing pictures to a repository.

Install `git-lfs` with `sudo apt-get install git-lfs`, then clone the project.

## Further Reading

* [Managing Execution](https://quarto.org/docs/projects/code-execution.html)
* [Project Scripts](https://quarto.org/docs/projects/scripts.html)
* [Project Profiles](https://quarto.org/docs/projects/profiles.html)
* [Gitlab Pages Deployment](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ui.html)