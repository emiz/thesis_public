#!/bin/bash

#Run this to move assets over which should be checked into git so thesis CI can render
#Note LZ repo requires LZ gitlab access, LLNL repo requires LLNL gitlab access

# Keep connection to LLNL gitlab open with:
# ssh -f -L 7999:your_user@czgitlab.llnl.gov:7999 your_user@oslic.llnl.gov sleep 12h

#Usage: 
# mkdir .deps
# cd .deps
# git clone git@gitlab.com:luxzeplin/analysis/alpaca/modules/sv_SEdecaytime.git
# git clone ssh://git@czgitlab.llnl.gov:7999/nlwg/users/emiz/daqman2py.git
# git clone ssh://git@czgitlab.llnl.gov:7999/nlwg/chillax/vapor-pressures.git

rsync -azh .deps/sv_SEdecaytime/docs/content/ebg_review/figs/ content/ch2/figs

rsync -azh .deps/sv_SEdecaytime/docs/content/DEB_algorithm/figs/ content/ch3/figs
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_in_TPC_regions/figs/ content/ch3/figs
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_SE_and_ME/figs/ content/ch3/figs

rsync -azh .deps/daqman2py/daqman2py/evalrois/figs/ content/ch4/figs

rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_in_TPC_regions/etrains_in_ext_liq.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_in_TPC_regions/drift_dep_history.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_in_TPC_regions/emiz_fields.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_in_TPC_regions/data_quality.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/DEB_algorithm/DEB_algorithm.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_SE_and_ME/se_me_trains.ipynb content/ch3
rsync -azh .deps/sv_SEdecaytime/docs/content/etrains_SE_and_ME/se_photoionization.ipynb content/ch3

rsync -azh .deps/daqman2py/daqman2py/evalrois/run_15.1_spe_gain_curves.ipynb content/ch4
rsync -azh .deps/daqman2py/daqman2py/getse/gette.ipynb content/ch4
rsync -azh .deps/daqman2py/daqman2py/getse/getse.ipynb content/ch4
rsync -azh .deps/daqman2py/daqman2py/serate/serate.ipynb content/ch4

rsync -azh .deps/vapor-pressures/*.ipynb content/ch5