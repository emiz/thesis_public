#!/bin/bash

# Updates the changelog files

# Prepend contents using cat and redirection
cat "content/changelog/_new_changes.qmd" "content/changelog/_old_changes.qmd" > "content/changelog/tmpch.tmp"

# Overwrite old changelod
mv "content/changelog/tmpch.tmp" "content/changelog/_old_changes.qmd"

# Delete the original file to prepend
git-cliff --unreleased --tag "$1" -o content/changelog/_new_changes.qmd