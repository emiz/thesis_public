#!/bin/bash

# Check if this has been set, if not set a default value
CI_COMMIT_TAG=${CI_COMMIT_TAG:-"no_ci_tag"}

# Base File Name
thesis_name="emiz_thesis" # must be same as book["output-file"] in _quarto-thesis-.yml
new_thesis_name="$(date +%Y-%m-%d)_${thesis_name}_${CI_COMMIT_TAG}"

# Change thesis pdf name
mv public/${thesis_name}.pdf public/${new_thesis_name}.pdf

# find replace in html hrefs so download links work
find "public" -name "*.html" -exec sed -i "s/${thesis_name}/${new_thesis_name}/g" {} +

# Archive outputs needed to render the pdf
# Note that quarto will convert svgs to pdfs
tar -czvf public/${new_thesis_name}.tar.gz $(find content -type f -name "*.jpg" -o -name "*.png" -o -name "*.jpeg") index_files tex/references.bib ${thesis_name}.tex
