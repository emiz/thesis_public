#!/bin/bash

# Generate svg from mmd with drawn fonts instead of embedded so it works in pdf

# Store the input file path in a variable
input_file="$1"

# Extract the directory path and filename without the extension
input_dir=$(dirname "$input_file")
filename=$(basename "$input_file" .mmd)

# Step 1: Generate the PDF from the MMD file
# Use neutral theme and fit pdf size to figure
mmdc -i "$input_file" -o "$input_dir/$filename.pdf" -t neutral -f

# Step 2: Convert the PDF to SVG; this preserves fonts for inclusion in LaTeX
pdf2svg "$input_dir/$filename.pdf" "$input_dir/$filename.svg"

# Clean up the intermediate PDF file
rm "$input_dir/$filename.pdf"
