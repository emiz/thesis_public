#!/bin/bash

sudo apt update
sudo apt upgrade

# Install rsvg convert so quarto can convert svgs to pdfs for
# inclusion in pdf rendering
# Also install pdf2svg so we can convert pdfs for figures to svg
sudo apt install -y librsvg2-2 librsvg2-bin pdf2svg

# Check if quarto is installed
if dpkg -s quarto >/dev/null 2>&1; then
    echo "Package quarto is installed."
else
    export QUARTO_VERSION="1.4.550"
    sudo curl -o quarto-linux-amd64.deb -L https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-amd64.deb
    sudo dpkg -i quarto-linux-amd64.deb
    quarto install tinytex
    rm -f quarto-linux-amd64.deb
fi

# Install NVM to manage installations of NPM which is needed to install mermaid
# for making diagrams
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
source ~/.bashrc
nvm install node
npm install -g @mermaid-js/mermaid-cli
