#!/bin/bash

# rescale svgs so they don't look so small in lightboxes
# note this will basically destroy any information in them
rsvg-convert -z 2 -f svg -o "$1" "$1"

# The compress back down with svgo
svgo "$1"